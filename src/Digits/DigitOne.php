<?php

namespace Digits;

final class DigitOne implements \DigitInterface
{


    public function getTop():array
    {
        return [0,0,0];
    }

    public function getMiddle():array
    {
        return [0,0,1];
    }

    public function getBottom():array
    {
        return [0,0,1];
    }
}