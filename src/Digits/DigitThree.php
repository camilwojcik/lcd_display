<?php

namespace Digits;

final class DigitThree implements \DigitInterface
{

    public function getTop():array
    {
        return [0,1,0];
    }

    public function getMiddle():array
    {
        return [0,1,1];
    }

    public function getBottom():array
    {
        return [0,1,1];
    }
}