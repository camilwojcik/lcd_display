<?php

interface DigitInterface
{
    public function getTop():array;
    public function getMiddle():array;
    public function getBottom():array;
}