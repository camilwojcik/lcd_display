<?php

final class DigitFabric
{
    /**
     * @throws Exception
     */
    public static function getDigit(int $digit) : DigitInterface
    {
        switch ($digit){
            case 0:
                return new \Digits\DigitZero();
                break;
            case 1:
                return new Digits\DigitOne();
                break;
            case 2:
                return new Digits\DigitTwo();
                break;
            case 3:
                return new Digits\DigitThree();
                break;
            case 4:
                return new Digits\DigitFour();
                break;
            case 5:
                return new Digits\DigitFive();
                break;
            case 6:
                return new Digits\DigitSix();
                break;
            case 7:
                return new Digits\DigitSeven();
                break;
            case 8:
                return new Digits\DigitEight();
                break;
            case 9:
                return new Digits\DigitNine();
                break;
        }

        throw new Exception("Unknown digit type!");
    }
}