<?php

class DigitCollection implements \Countable
{
    private array $digits;

    /**
     * @param DigitInterface ...$digits
     */
    public function __construct(DigitInterface ...$digits)
    {
        $this->digits = $digits;
    }

    /**
     * @param DigitInterface $digit
     * @return void
     */
    public function add(DigitInterface $digit):void
    {
        $this->digits[] = $digit;
    }

    /**
     * @return array DigitInterface
     */
    public function all() :array
    {
        return $this->digits;
    }

    public function count():int
    {
        return count($this->digits);
    }
}