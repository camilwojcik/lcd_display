<?php

class LcdDisplay
{
    public function __construct(
        public string $new_line_char = '<br/>',
        public string $spacing_char = '&nbsp;',
        public string $vertical_char = '|',
        public string $horizontal_char = '_'
    ){}

    public function printDigits(DigitCollection $digits):void
    {
        //each digit has 3 spaces where in the middle there's __ and otherwise it`s | on left or right
        //  _     _  _     _  _  _   _  _
        // | |  | _| _||_||_ |_   | |_||_|
        // |_|  ||_  _|  | _||_|  | |_| _|

        //horizontal 0  => // 0 - 1 - 0 / 1 - 0 - 1 / 1 - 1 - 1
        // 0 - 1 - 0
        // 1 - 0 - 1
        // 1 - 1 - 1
        //
        //  foreach -> gettop + getmid + getBot -> iterate + "\n" every x * 3


        $digits_n = $digits->count();
        $map = $this->getDigitsMap($digits);
        $digit_index = 0;
        foreach (str_split($map) as $k => $v) {
            if($v != 0)
            {
                if($digit_index === 1){
                    echo $this->horizontal_char;
                }else{
                    echo $this->vertical_char;
                }
            }else{
                echo $this->spacing_char;
            }

            if($digit_index === 2){
                echo $this->spacing_char;
                $digit_index = 0;
            }else{
                $digit_index++;
            }

            if(($k+1) % ($digits_n * 3) == 0)
            {
                echo $this->new_line_char;
            }
        }
    }

    /**
     * @param DigitCollection $digits
     * @return string
     */
    protected function getDigitsMap(DigitCollection $digits):string{
        $top_row = $mid_row = $bottom_row = '';
        foreach ($digits->all() as $digit){
            $top_row .=  implode("", $digit->getTop());
            $mid_row .= implode("", $digit->getMiddle());
            $bottom_row .= implode("", $digit->getBottom());
        }
        return $top_row . $mid_row . $bottom_row;
    }
}