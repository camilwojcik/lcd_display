<?php

include "src/LcdDisplay.php";
include "src/DigitCollection.php";
include "src/DigitFabric.php";
include "src/DigitInterface.php";
foreach (glob("src/Digits/*.php") as $filename)
{
    include $filename;
}
echo "<div>";
$printer = new LcdDisplay();
$digits = new DigitCollection();
$digits->add(DigitFabric::getDigit(0));
$digits->add(DigitFabric::getDigit(1));
$digits->add(DigitFabric::getDigit(2));
$digits->add(DigitFabric::getDigit(3));
$digits->add(DigitFabric::getDigit(4));
$digits->add(DigitFabric::getDigit(5));
$digits->add(DigitFabric::getDigit(6));
$digits->add(DigitFabric::getDigit(7));
$digits->add(DigitFabric::getDigit(8));
$digits->add(DigitFabric::getDigit(9));
$printer->printDigits($digits);
echo "<div/>";

